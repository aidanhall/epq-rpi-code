
def report_only_new_items(old, new):
    only_new_items = []
    for item in new:
        if not item in old and not item in only_new_items:
            only_new_items.append(item)
    return only_new_items

new_items = [1, 5, 4, 5, 3]
old_items = [1, 3]
print(report_only_new_items(old_items, new_items))
