#!/usr/bin/python3

import time
from RPLCD import CharLCD
import RPi.GPIO as GPIO

lcd = CharLCD(cols=16, rows=2, pin_rs=31, pin_e=29, pins_data=[24,21,19,23], numbering_mode=GPIO.BOARD)

lcd.clear()
lcd.cursor_pos=(0,0)

text="This is some scrolling text."


for i in range(len(text)):
    if (i+16 >= len(text)):
        end = len(text)
        lcd.clear()
    else:
        end=i+16
    subs = text[i:end]
    lcd.cursor_pos = (0,0)
    lcd.write_string(str(subs))
    time.sleep(0.5)
lcd.clear()
GPIO.cleanup()
