#!/usr/bin/python3
import time

from RPi import GPIO
from RPLCD.gpio import CharLCD

lcd = CharLCD(numbering_mode=GPIO.BCM, pin_rs=11, pin_rw=9, pin_e=10, pins_data=[22, 23, 24, 25], auto_linebreaks=True)

lcd.clear()
lcd.write_string("hello world")
time.sleep(2)
GPIO.cleanup()
