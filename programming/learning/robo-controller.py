#!/usr/bin/python3

from evdev import InputDevice, categorize, ecodes
from gpiozero import Robot
from gpiozero import LED
import RPi.GPIO

bBtn = 304
aBtn = 305
yBtn = 306
xBtn = 307
homeBtn = 316

controllerId = input("Enter input event number (default 0): ")
if (controllerId == ""):
    controllerId = '/dev/input/event0'
else:
    controllerId = '/dev/input/event' + controllerId
gamepad = InputDevice(controllerId)
robot = Robot((20,26), (16,19))
led = LED(2)

print(gamepad)
robot.stop()
for event in gamepad.read_loop():
    if (event.code != 0 and event.code != 1):
        print(categorize(event))
        print(event.code, event.value)

    if (event.type == ecodes.EV_KEY):
        if (event.value == 1):
            if (event.code == bBtn):
                robot.forward()
            elif (event.code == aBtn):
                robot.stop()
            elif (event.code == yBtn):
                robot.backward()
            elif (event.code == xBtn):
                led.toggle()
        elif (event.value == 0):
            if (event.code == bBtn or event.code == yBtn):
                robot.stop()
        if (event.code == homeBtn):
            RPi.GPIO.cleanup()
            exit()
    if (event.code == 17):
        if (event.value == 1):
            robot.forward()
        elif(event.value == 0):
            robot.stop()
        elif(event.value == -1):
            robot.backward()
    if (event.code == 16):
        if (event.value == -1):
            robot.left()
        elif(event.value == 0):
            robot.stop()
        elif(event.value == 1):
            robot.right()
