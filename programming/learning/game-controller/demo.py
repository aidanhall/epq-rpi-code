#!/usr/bin/python3

from evdev import InputDevice, categorize, ecodes

bBtn = 304
aBtn = 305

gamepad = InputDevice('/dev/input/event0')

print(gamepad)

for event in gamepad.read_loop():
    print(categorize(event))

    if (event.type == ecodes.EV_KEY):
        if (event.code == aBtn):
            print(event)
        if (event.code == bBtn):
            print(event)
