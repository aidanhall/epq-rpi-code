from inputs import get_key
from time import sleep
from gpiozero import Motor

speed_mod = 1.0
speed = 0

class FancyRobot(object):
    def __init__(self, left_pins, right_pins, speed_mod):
        self.motors = [Motor(left_pins[0], left_pins[1]), Motor(right_pins[0], right_pins[1])]
        # This is a modifier which sets the maximum speed of each motor.
        self.speed_mod = speed_mod
        # These range from -1 to 1, determining the speed of each motor.
        self.speeds = [0,0]
        self.stop()

    def stop(self):
        self.set_update([0.0, 0.0])

    def set_update(self, new_speeds):
        self.set_speeds(new_speeds)
        self.update_speeds()


    def update_speeds(self):
        """Use the current values in speeds[] to set the speeds of the motors."""
        for i in range(len(self.motors)):
            print(self.speeds[i])
            if (self.speeds[i] < 0.0):
                self.motors[i].backward(-self.speeds[i] * self.speed_mod)
            elif (self.speeds[i] == 0.0):
                self.motors[i].stop()
            else:
                self.motors[i].forward(self.speeds[i] * self.speed_mod)
            
    def change_speeds(self, delta_speeds):
        for i in range(len(delta_speeds)):
            self.speeds[i] += delta_speeds[i]
            if self.speeds[i] > 1:
                self.speeds[i] = 1

            elif self.speeds[i] < -1:
                self.speeds[i] = -1


    def set_speeds(self, new_speeds):
        for i in range(len(new_speeds)):
            if abs(new_speeds[i] > 1.0):
                print("Speeds must vary between 1 and -1.")
                new_speeds[i] /= abs(new_speeds[i])
            self.speeds[i] = new_speeds[i]
        

        

def main():
    robo = FancyRobot((18, 23), (17, 22), 1.0)
    """
    for i in range(0, 3):
        robo.change_speeds([0.4, 0.4])
        robo.update_speeds()
        sleep(1)
    robo.stop()
    """
    udlr = [False, False, False, False]
    while (1):
        speed = 0
        events = get_key()
        # Allows robot to be controlled with the arrow keys.
        robo.change_speeds([0.0, 0.0])
        for event in events:
            #print(event.ev_type, event.code, event.state)
            if event.code == 'KEY_UP':
                if event.state == 1:
                    udlr[0] = True
                if event.state == 0:
                    udlr[0] = False
            if event.code == 'KEY_DOWN':
                if event.state == 1:
                    udlr[1] = True
                if event.state == 0:
                    udlr[1] = False
            if event.code == 'KEY_LEFT':
                if event.state == 1:
                    udlr[2] = True
                if event.state == 0:
                    udlr[2] = False
            if event.code == 'KEY_RIGHT':
                if event.state == 1:
                    udlr[3] = True
                if event.state == 0:
                    udlr[3] = False

        if udlr[0]:
            robo.change_speeds([1.0, 1.0])
        if udlr[1]:
            robo.change_speeds([-1.0, -1.0])
        if udlr[2]:
            robo.change_speeds([0.5, -0.5])
        if udlr[3]:
            robo.change_speeds([-0.5, 0.5])
        robo.update_speeds()






if __name__ == "__main__":
    main()
