from gpiozero import Robot
from time import sleep

robot = Robot((19,16) , (20,26))

robot.forward()
sleep(1)
robot.backward()
sleep(1)
robot.left()
sleep(1)
robot.right()
sleep(1)
robot.stop()
