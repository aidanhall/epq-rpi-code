from gpiozero import Button, LED, Buzzer
from signal import pause

button = Button(2)
led = LED(17)
buzzer = Buzzer(21)

def enable_outputs():
    led.blink(0.2, 0.2)
    buzzer.on()

def disable_outputs():
    led.off()
    buzzer.off()

button.when_pressed = enable_outputs
button.when_released = disable_outputs

pause()
