# python3
#
# This file is adapted from the following example:
# https://github.com/tensorflow/examples/tree/master/lite/examples/object_detection/raspberry_pi/detect_picamera.py
#
# Each function/code block from the example that has been used/adapted is clearly marked.
# The code used is subject to the following license:
#
# Copyright 2019 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Example using TF Lite to detect objects with the Raspberry Pi camera."""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import io
import re
import time


import numpy as np
import picamera

from PIL import Image
from tflite_runtime.interpreter import Interpreter

CAMERA_WIDTH = 640
CAMERA_HEIGHT = 480

detection_types = [
        [ 'harmful' , ['cell phone', 'laptop', 'remote', 'keyboard']],
        [ 'person', ['person']]
        ]

# Copied verbatim from example:
def load_labels(path):
  """Loads the labels file. Supports files with or without index numbers."""
  with open(path, 'r', encoding='utf-8') as f:
    lines = f.readlines()
    labels = {}
    for row_number, content in enumerate(lines):
      pair = re.split(r'[:\s]+', content.strip(), maxsplit=1)
      if len(pair) == 2 and pair[0].strip().isdigit():
        labels[int(pair[0])] = pair[1].strip()
      else:
        labels[row_number] = pair[0].strip()
  return labels


# Copied verbatim from example:
def set_input_tensor(interpreter, image):
  """Sets the input tensor."""
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image


# Copied verbatim from example:
def get_output_tensor(interpreter, index):
  """Returns the output tensor at the given index."""
  output_details = interpreter.get_output_details()[index]
  tensor = np.squeeze(interpreter.get_tensor(output_details['index']))
  return tensor


# Copied verbatim from example:
def detect_objects(interpreter, image, threshold):
  """Returns a list of detection results, each a dictionary of object info."""
  set_input_tensor(interpreter, image)
  interpreter.invoke()

  # Get all output details
  boxes = get_output_tensor(interpreter, 0)
  classes = get_output_tensor(interpreter, 1)
  scores = get_output_tensor(interpreter, 2)
  count = int(get_output_tensor(interpreter, 3))

  results = []
  for i in range(count):
    if scores[i] >= threshold:
      result = {
          'bounding_box': boxes[i],
          'class_id': classes[i],
          'score': scores[i]
      }
      results.append(result)
  return results


# Based on the annotate_objects function in the example:
def print_objects(results, labels):
  for obj in results:
    # Convert the bounding box figures from relative coordinates
    # to absolute coordinates based on the original resolution
    ymin, xmin, ymax, xmax = obj['bounding_box']
    xmean = (xmin + xmax) / 2
    ymean = (ymin + ymax) / 2

    for group in detection_types:
        if (labels[obj['class_id']] in group[1]):
            print('Detected (%s): %s\t%.2f' % (group[0], labels[obj['class_id']], obj['score']))
        # else:
        #     print('Detected: %s\t%.2f' % (labels[obj['class_id']], obj['score']))



# Based on the main loop of the example.
def detector_main():
    threshold = 0.4
    labels = load_labels("coco_labels.txt")
    interpreter = Interpreter("detect.tflite")

    # Interpreter preparation copied verbatim from example:
    interpreter.allocate_tensors()
    _, input_height, input_width, _ = interpreter.get_input_details()[0]['shape']

    # Camera loading and try-finally structure adapted from example.
    with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
        try:
            stream = io.BytesIO()

            for _ in camera.capture_continuous(stream, format='jpeg', use_video_port=True):
                stream.seek(0)
                image = Image.open(stream).convert('RGB').resize(
                (input_width, input_height), Image.ANTIALIAS)
                start_time = time.monotonic()
                results = detect_objects(interpreter, image, threshold)
                elapsed_ms = (time.monotonic() - start_time) * 1000

                print_objects(results, labels)


                stream.seek(0)
                stream.truncate()

        finally:
            pass

if __name__ == '__main__':
    detector_main()
