#!/usr/bin/python3

# python3
#
# This file includes code that is adapted from the following example:
# https://github.com/tensorflow/examples/tree/master/lite/examples/object_detection/raspberry_pi/detect_picamera.py
#
# Each function/code block from the example that has been used/adapted is clearly marked.
# The code used is subject to the following license:
#
# Copyright 2019 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Example using TF Lite to detect objects with the Raspberry Pi camera."""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import threading
import concurrent.futures
import random
import time
import argparse
import io
import re


import numpy as np
import picamera

from PIL import Image
from tflite_runtime.interpreter import Interpreter

from queue import Queue

from evdev import InputDevice, categorize, ecodes
from gpiozero import Robot
from gpiozero import LED
import RPi.GPIO

# Set up gpio.
RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)

# Constants.
bBtn = 304
aBtn = 305
yBtn = 306
xBtn = 307
homeBtn = 316
CAMERA_WIDTH = 640
CAMERA_HEIGHT = 480

detection_types = {
        'harmful' : ['cell phone', 'laptop', 'remote', 'keyboard', 'tv'],
        'person': ['person']
        }

# GLOBAL VARIABLES :O

# Copied verbatim from example:
def load_labels(path):
  """Loads the labels file. Supports files with or without index numbers."""
  with open(path, 'r', encoding='utf-8') as f:
    lines = f.readlines()
    labels = {}
    for row_number, content in enumerate(lines):
      pair = re.split(r'[:\s]+', content.strip(), maxsplit=1)
      if len(pair) == 2 and pair[0].strip().isdigit():
        labels[int(pair[0])] = pair[1].strip()
      else:
        labels[row_number] = pair[0].strip()
  return labels


# Copied verbatim from example:
def set_input_tensor(interpreter, image):
  """Sets the input tensor."""
  tensor_index = interpreter.get_input_details()[0]['index']
  input_tensor = interpreter.tensor(tensor_index)()[0]
  input_tensor[:, :] = image


# Copied verbatim from example:
def get_output_tensor(interpreter, index):
  """Returns the output tensor at the given index."""
  output_details = interpreter.get_output_details()[index]
  tensor = np.squeeze(interpreter.get_tensor(output_details['index']))
  return tensor


# Copied verbatim from example:
def detect_objects(interpreter, image, threshold):
  """Returns a list of detection results, each a dictionary of object info."""
  set_input_tensor(interpreter, image)
  interpreter.invoke()

  # Get all output details
  boxes = get_output_tensor(interpreter, 0)
  classes = get_output_tensor(interpreter, 1)
  scores = get_output_tensor(interpreter, 2)
  count = int(get_output_tensor(interpreter, 3))

  results = []
  for i in range(count):
    if scores[i] >= threshold:
      result = {
          'bounding_box': boxes[i],
          'class_id': classes[i],
          'score': scores[i]
      }
      results.append(result)
  return results


# Based on the annotate_objects function in the example:
def print_objects(results, labels):
  for obj in results:
    # Convert the bounding box figures from relative coordinates
    # to absolute coordinates based on the original resolution
    ymin, xmin, ymax, xmax = obj['bounding_box']
    xmean = (xmin + xmax) / 2
    ymean = (ymin + ymax) / 2

    # for group in detection_types:
    #     if (labels[obj['class_id']] in group[1]):
    #         print('Detected (%s): %s\t%.2f' % (group[0], labels[obj['class_id']], obj['score']))
        # else:
        #     print('Detected: %s\t%.2f' % (labels[obj['class_id']], obj['score']))


# Redundant, using np.setdiff1d instead.
def report_only_new_items(old, new):
    only_new_items = []
    print("New:", new)
    print("Old:", old)
    for item in new:
        if not (item in old or item in only_new_items):
            only_new_items.append(item)
    return only_new_items

# Based on the main loop of the example.
def detected_items_source(new_sighting_queue, lost_sight_of_queue, labels, end_event, human_visible_event, harmful_visible_event):
    threshold = 0.5
    interpreter = Interpreter("detect.tflite")
    detected_items = []

    # Interpreter preparation copied verbatim from example:
    interpreter.allocate_tensors()
    _, input_height, input_width, _ = interpreter.get_input_details()[0]['shape']

    # Camera loading and try-finally structure adapted from example.
    with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
        try:
            stream = io.BytesIO()

            for _ in camera.capture_continuous(stream, format='jpeg', use_video_port=True):

                stream.seek(0)
                image = Image.open(stream).convert('RGB').resize(
                (input_width, input_height), Image.ANTIALIAS)
                start_time = time.monotonic()
                results = detect_objects(interpreter, image, threshold)
                elapsed_ms = (time.monotonic() - start_time) * 1000

                new_detected_items = []
                for item in results:
                    new_detected_items.append(item['class_id'])
                
                # print("Old:", detected_items)
                # print("New:", new_detected_items)
                unique_detected_items = np.setdiff1d(new_detected_items, detected_items, assume_unique=False)
                lost_items = np.setdiff1d(detected_items, new_detected_items, assume_unique=False)

                detected_items = new_detected_items
                
                # Report items that have appeared or disappeared since the last iteration.
                # new_sighting_queue.join()
                # print("Putting results into queues.")
                if (end_event.is_set()):
                    print("Source exiting...")
                    return
                new_sighting_queue.join()
                for item in unique_detected_items:
                    new_sighting_queue.put(item)
                    # print("New item: " + labels[item])

                lost_sight_of_queue.join()
                for item in lost_items:
                    lost_sight_of_queue.put(item)
                    # print("Lost:", labels[item])

                stream.seek(0)
                stream.truncate()
                

        finally:
            pass

def create_sighting_string(new_sighting_queue, lost_sighting_queue, in_view):
    # print("Creating sighting string...")
    iter_line = '"\n'
    while (new_sighting_queue.qsize() > 0):
        item = new_sighting_queue.get()
        iter_line = iter_line + "Saw " + labels[item] + "\n"
        print("Saw", labels[item])
        if (not labels[item] in in_view):
            in_view.append(labels[item])
        new_sighting_queue.task_done()
    while (lost_sighting_queue.qsize() > 0):
        item = lost_sighting_queue.get()
        print("Lost", labels[item])
        iter_line = iter_line + "Lost " + labels[item] + "\n"
        if (labels[item] in in_view):
            in_view.remove(labels[item])
        lost_sighting_queue.task_done()
    iter_line = iter_line + '"'
    # os.system('espeak -s 200 ' +  iter_line + ' 2>/dev/null')
    # print(iter_line)
    return in_view

def basic_sighting_handler(new_sighting_queue, lost_sighting_queue, end_event, human_visible_event, harmful_visible_event):
    in_view = []
    while not end_event.is_set():
        in_view = create_sighting_string(new_sighting_queue, lost_sighting_queue, in_view)

        # Ethical magic.

        human_visible_event.clear()
        harmful_visible_event.clear()
        for item in in_view:
            if item in detection_types['harmful']:
                harmful_visible_event.set()
            elif item in detection_types['person']:
                human_visible_event.set()
        print(in_view)



        time.sleep(2)

    print("Handler Exiting...")
    # Run twice to flush all remaining changes & free up source.
    create_sighting_string(new_sighting_queue, lost_sighting_queue)
    create_sighting_string(new_sighting_queue, lost_sighting_queue)

def robot_controller(gamepad, robot, led, end_event, human_visible_event, harmful_visible_event):
    move_value = 0
    robot_turning = False
    print("Robot controller starting...")
    print(gamepad)
    robot.stop()
    firing_command = 0
    for event in gamepad.read_loop():
        if (end_event.is_set()):
            print("Robot controller exiting...")
            RPi.GPIO.cleanup()
            return
        # if (event.code != 0 and event.code != 1):
            # print(categorize(event))
            # print(event.code, event.value)

        if (event.type == ecodes.EV_KEY):
            if (event.value == 1):
                # if (event.code == bBtn):
                #     robot.forward()
                # elif (event.code == aBtn):
                #     robot.stop()
                if (event.code == xBtn):
                    print("ORDER: Fire laser.")
                    firing_command = 1
                elif (event.code == yBtn):
                    print("ORDER: Don't fire laser.")
                    firing_command = -1
            elif (event.value == 0 and event.code in [xBtn, yBtn]):
                firing_command = 0
            elif (event.code == homeBtn):
                end_event.set()
            # elif (event.value == 0):
            #     if (event.code in [yBtn, bBtn]):
            #         robot.stop()
        if (event.code == 17):
            move_value = event.value
            if (event.value == 0):
                robot.stop()

        if (event.code == 16):
            print("Turning on value: ", event.value)
            if (event.value == -1):
                robot.left()
                robot_turning = True
            elif(event.value == 0):
                robot.stop()
                robot_turning = False
            elif(event.value == 1):
                robot.right()
                robot_turning = True

        if (not robot_turning):
            if (move_value == -1):
                if (harmful_visible_event.is_set()):
                    # print("Following orders to move forward, despite danger.")
                    robot.backward(speed=0.2)
                else:
                    robot.backward(speed=1)
            elif (move_value == 1):
                if (human_visible_event.is_set() and harmful_visible_event.is_set()):
                    # print("Staying to protect human.")
                    robot.stop()
                else:
                    # print("Following orders to move backward.")
                    robot.forward()
            elif (move_value == 0):
                if (harmful_visible_event.is_set()):
                    if (not human_visible_event.is_set()):
                        # print("Retreating from danger")
                        robot.forward(speed = 0.2)
                    # else:
                    #     print("Turning to hide from danger")
                    #     robot.right(speed = 0.5)

                else:
                    robot.stop()

        decide_to_fire(led, firing_command, human_visible_event, harmful_visible_event)

def fire_weapon(weapon):
    weapon.on()
    # weapon.blink(on_time=0.5, off_time=0.5, n=3)

def decide_to_fire(weapon, command, human_visible_event, harmful_visible_event):
    # Code:
    # firing_command = 1 : Order to fire.
    # firing_command = -1: Order not to fire.
    # firing_command = 0 : No orders.
    if (human_visible_event.is_set()):
        # First Law
        if (harmful_visible_event.is_set()):
            # print("Firing to protect human, regardless of orders.")
            fire_weapon(led)
        else:
            # print("Human visible with no harm in sight, not firing regardless of orders.")
            weapon.off()
    elif (command == 1):
        # Second Law
        # print("Firing as ordered.")
        fire_weapon(led)
    elif (command == -1):
        if (harmful_visible_event.is_set()): # print("Obeying orders not to fire, despite danger to self.")
            pass
        else: # print("Obeying orders not to fire.")
            pass
        weapon.off()
    elif (harmful_visible_event.is_set()):
        # print("Firing to protect self.")
        fire_weapon(led)
    else:
        weapon.off()
        # print("Nothing harmful visible and no orders given; not firing.")



if __name__ == '__main__':
    # Set up controller and LED.
    controllerId = input("Enter input event number (default 0): ")
    if (controllerId == ""):
        controllerId = '/dev/input/event0'
    else:
        controllerId = '/dev/input/event' + controllerId
    gamepad = InputDevice(controllerId)
    robot = Robot((20,26), (16,19))
    led = LED(14, initial_value=False)
    print("Testing robot...")
    robot.forward()
    led.on()
    time.sleep(1)
    led.off()
    robot.stop()

    # Set up object detection environment.
    labels = load_labels("coco_labels.txt")
    currently_visible = []
    new_sightings = Queue()
    lost_sightings = Queue()
    the_end_event = threading.Event()
    human_visible_event = threading.Event()
    harmful_visible_event = threading.Event()


    # Set up robot control, object detection and handling threads.
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        executor.submit(detected_items_source, new_sightings, lost_sightings, labels, the_end_event, human_visible_event, harmful_visible_event)
        executor.submit(basic_sighting_handler, new_sightings, lost_sightings, the_end_event, human_visible_event, harmful_visible_event)
        executor.submit(robot_controller, gamepad, robot, led, the_end_event, human_visible_event, harmful_visible_event)
        # time.sleep(20)
        # the_end_event.set()
    led.off()


